import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit {
  @ViewChild('videoElement') videoElement!: ElementRef; // Usar el operador de aserción de no null
  videoWidth = 0;
  videoHeight = 0;
  constraints = {
    video: {
      facingMode: 'user', // 'environment' para la cámara trasera
      width: { ideal: 1280 },
      height: { ideal: 720 }
    }
  };

  constructor() { }

  ngOnInit() {
    this.startCamera();
  }

  startCamera() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(stream => {
        this.videoElement.nativeElement.srcObject = stream;
        this.videoElement.nativeElement.play();
      }).catch(error => {
        console.error('Error accessing the camera', error);
      });
    } else {
      alert('Sorry, camera not available.');
    }
  }
}
