import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  latitude: number | undefined;
  longitude: number | undefined;
  errorMessage: string | undefined;

  constructor() { }

  ngOnInit(): void {
    this.getLocation();
  }

  getLocation(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
        },
        (error: GeolocationPositionError) => {
          switch(error.code) {
            case error.PERMISSION_DENIED:
              this.errorMessage = "User denied the request for Geolocation.";
              break;
            case error.POSITION_UNAVAILABLE:
              this.errorMessage = "Location information is unavailable.";
              break;
            case error.TIMEOUT:
              this.errorMessage = "The request to get user location timed out.";
              break;
            default:
              this.errorMessage = "An unknown error occurred.";
              break;
          }
        }
      );
    } else {
      this.errorMessage = "Geolocation is not supported by this browser.";
    }
  }
}
